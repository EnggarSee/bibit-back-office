import React from 'react'
import { Collapse, Select, Checkbox } from 'antd';

const { Panel } = Collapse;
const { Option } = Select;
class FilterContent extends React.Component {

    handleChange(value) {
        console.log(`selected ${value}`);
    }
    onChange(checkedValues) {
        console.log('checked = ', checkedValues);
      }
    onChange2(checkedValues) {
        console.log('checked = ', checkedValues);
      }
    

    render(){
        console.log(this.props, 'propss');
        return(
            <div className='filter-content-show-wrapper'>
                <div className='boundary-content'/>
                <Collapse defaultActiveKey={['']} ghost  expandIconPosition="right">
                    <Panel header="Paymen Method" key="1">
                        <ul class='list-filter'>
                            <li>Transfer</li>
                        </ul>
                    </Panel>
                    <div className='reksadana-collapse'>
                    <Collapse defaultActiveKey={['15']} expandIconPosition="right">
                        
                        <Panel key='15' className='filter-content-header' header='All Partner'>
                            <div className='partner-radio-sub-item'>
                            <Checkbox.Group defaultValue={['All']}>
                                <Checkbox >All</Checkbox>
                                <Checkbox value={false} onChange={this.onChange}>Bibit</Checkbox>
                                <Checkbox value={true} onChange={this.onChange2}>LinkAja</Checkbox>

                            </Checkbox.Group>
                            </div>
                        </Panel>
                    </Collapse>
                    </div>
                    <Panel header="Confirmed Transaction" key="2">
                        <ul class='list-filter'>
                            <li>Accepted</li>
                        </ul>
                    </Panel>
                    <div className='reksadana-collapse'>
                    <Collapse defaultActiveKey={['20']} expandIconPosition="right">
                    <Panel key='20'header='Reksadana'>
                        <div className='reksadana-collapse-item'>
                            <Collapse defaultActiveKey={['']} ghost  expandIconPosition="right">
                                <Panel header="Include Reksadana" key="1">
                                <Select placeholder="Option1" style={{ width: 150 }} onChange={this.handleChange}>
                                    <Option value="jack">Jack</Option>
                                    <Option value="lucy">Lucy</Option>
                                </Select>
                                </Panel>
                                <Panel header="Exclude Reksadana" key="2">
                                <Select placeholder="Option2" style={{ width: 150 }} onChange={this.handleChange}>
                                    <Option value="jack">Jack</Option>
                                    <Option value="lucy">Lucy</Option>
                                </Select>
                                </Panel>
                            </Collapse>
                        </div>
                    </Panel>
                    </Collapse>  
                </div>
                    <Panel header="Transaction Type" key="3">
                        <ul class='list-filter'>
                            <li>Online Transfer</li>
                        </ul>
                    </Panel>
                    <Panel header="MI" key="4">
                        <ul class='list-filter'>
                            <li>Sub Menu</li>
                        </ul>
                    </Panel>
                    <Panel header="PBy Approver / Rejector" key="5">
                        <ul class='list-filter'>
                            <li>Sub Menu</li>
                        </ul>
                    </Panel>
                    <Panel header="Confirmed Transaction" key="6">
                        <ul class='list-filter'>
                            <li>Sub Menu</li>
                        </ul>
                    </Panel>
                </Collapse>
            </div>
        );
    };
}
export default FilterContent;