import React from 'react'
import '../WaitingApproval/WaitingApproval.css'
import { Input, Select, Button, Dropdown, Space, DatePicker, Table, Pagination } from 'antd';
import reload from '../../../assets/reload-icon.svg'
import { SearchOutlined, DownOutlined } from '@ant-design/icons'
import tableList from '../../DataTable/WaitingApprovalList'
import tableData from '../../DataTable/WaitingApprovalData'
import listPagination from '../../../components/listPagination'
import { useHistory } from "react-router-dom";

const { Option } = Select



function WaitingApproval(){

    let history = useHistory();

    

    
        return(
            <div className='waiting-approval-page'>
                <div className='content-header'>
                    <div className='left-content'>
                        <p>Waiting CS Approval</p>
                        <span>&#8226;</span>
                        <p>12 Total User</p>
                    </div>
                    <div className='right-content'>
                        <img src={reload} alt=""/>
                        <p>Refresh</p>
                    </div>
                </div>
                <div className='content-table-header'>
                    <div className='left-content'>
                        <p>Search</p>
                    </div>
                    <div className='right-content'>
                    <div className='left-filter-content'>
                        <Select placeholder="Phone" style={{ width: 85 }} >
                        <Option value="jack">Jack</Option>
                        <Option value="lucy">Lucy</Option>
                        </Select>
                    </div>
                    <div className='boundary-content' />

                    <Input placeholder='Search for name, invoice, etc' prefix={<SearchOutlined/>}></Input>
                    <div className='boundary-content' />
                    <Space direction="vertical" size={12}>
                        <DatePicker placeholder="Date"/>
                    </Space>
                    <div className='right-filter-content'>
                        <Button type='primary'>Search</Button>
                    </div>
                    </div>
                </div>
                <div className='content-table-body'>
                    <Table 
                        columns={tableList}  
                        dataSource={tableData} 
                        scroll={{ y: 300 }}
                        onRow={(record, recordIndex) => ({
                            onClick: event  => history.push("/waiting-approval-detail") 
                          })}
                    />
                </div>
                <div className='content-table-pagination'>
                    <Pagination
                        showSizeChanger
                        defaultCurrent={1}
                        total={100}
                    />
                    <Dropdown overlay={listPagination} placement="bottomCenter"  arrow>
                        <Button>10<DownOutlined /></Button>
                    </Dropdown>
                    </div>
            </div>
        )
}

export default WaitingApproval