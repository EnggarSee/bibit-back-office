import React, { useState } from 'react'
import '../WaitingApproval/WaitingApproval.css'

import { Button, Select, Tabs, Form, Input, Modal } from 'antd'
import Reject from '../../../assets/reject-icon.svg'
import Approve from '../../../assets/accept-icon.svg'
import KTP from '../../../assets/ktp.png'
import Upload from '../../../assets/upload-icon.svg'
import Undo from '../../../assets/undo-icon.svg'
import Redo from '../../../assets/redo-icon.svg'
import ZoomOut from '../../../assets/zoom-out.svg'
import ZoomIn from '../../../assets/zoom-in.svg'
import { DeleteOutlined } from '@ant-design/icons';
import {ReactComponent as UploadInput} from '../../../assets/upload-input.svg'
import {ReactComponent as BackButton} from '../../../assets/back-button.svg'
import MainPhoto from '../../../assets/EDD1.png'
import SubPhoto1 from '../../../assets/EDD2.png'
import SubPhoto2 from '../../../assets/EDD3.png'
import Signature from '../../../assets/Singature.png'


const { Option } = Select;
const { TabPane } = Tabs;

function WaitingApprovalDetail(){
        const [OCR] = useState('12345') 
        const [isModalVisible, setIsModalVisible] = useState(false);
    

        const showModal = () => {
            setIsModalVisible(true);
        };
        
        const handleOk = () => {
            setIsModalVisible(false);
        };
        
        const handleCancel = () => {
            setIsModalVisible(false);
        };

        return(
            <div className='waiting-approval-detail'>
                <div className='content-header'>
                    <div className='left-content'>
                        <BackButton/>
                    </div>
                    <div className='right-content'>
                        <Button>Need Support</Button>
                        <Button>Open Crips</Button>
                        <Button>Update ES</Button>
                        <Button>Tag Attachment</Button>
                        <div className='status-wrapper'>
                            <p>Status:</p>
                            <Select defaultValue='PAID'>
                                <Option value='PAID'>PAID</Option>
                                <Option value='UNPAID'>UNPAID</Option>
                            </Select>
                        </div>
                    </div>
                </div>
                <div className='user-information'>
                    <div className='left-content'>
                        <div className='information-wrapper'>
                            <p>Payment Name:</p>
                            <p>Enggar Septrinas</p>
                        </div>
                        <div className='information-wrapper'>
                            <p>Registration Date:</p>
                            <p>Mar 31 2021, 07:39</p>
                        </div>
                        <div className='information-wrapper'>
                            <p>Nomor Handphone:</p>
                            <p>6281234567890</p>
                        </div>
                        <div className='information-wrapper'>
                            <p>Email:</p>
                            <p>enggar@stockbit.com</p>
                        </div>
                    </div>
                    <div className='right-content'>
                        <Button className='reject'><img src={Reject} alt=""/>REJECT</Button>
                        <Button className='approve'><img src={Approve} alt=""/>APPROVE</Button>
                    </div>
                </div>
                <div className='approval-detail'>
                    <div className='left-content'>
                        <div className='document-detail'>
                            <div className='document-header'>
                                <div className='left-header'>
                                    <p class='header active'>KTP</p>
                                    <p class='header'>EDD</p>
                                    <p class='header'>Signature</p>
                                </div>
                                <div className='right-header'>
                                    <p class='header' onClick={showModal}>Compare</p>
                                </div>
                            </div>
                            <div className='document-image'>
                                <img src={KTP} alt=""/>
                            </div>
                            <div className='document-control'>
                                <div className='left-content'>
                                    <img src={Upload} alt=""/>
                                </div>
                                <div className='right-content'>
                                    <img src={Undo} alt=""/>
                                    <img src={Redo} alt=""/>
                                    <img src={ZoomOut} alt=""/>
                                    <img src={ZoomIn} alt=""/>
                                </div>
                            </div>
                        </div>
                        <div className='document-footer'>
                            <p>Upload</p>
                            <div className='footer-list'>
                                <p>KTP</p>
                                <p>EDD</p>
                                <p>Signature</p>
                            </div>
                        </div>
                    </div>
                    <div className='right-content'>
                        <Tabs defaultActiveKey="1">
                            <TabPane tab="KTP Form Input" key='"1'>
                                <div className='form-input-wrapper'>
                                    <div className='form-input'>
                                        <Form.Item label="Nomor Identitas Hasil OCR">
                                            <Input value={OCR} style={OCR!=='' ? {backgroundColor: "#F9F9F9"} : {backgroundColor: '#fffff'}} placeholder="Dropdown" />
                                        </Form.Item>
                                        <Form.Item label="Nomor Identitas">
                                            <Input placeholder="Contoh : 1234567890" />
                                        </Form.Item>
                                        <div className='multiple-input'>
                                            <Form.Item label="Nama Awal">
                                                <Input placeholder="Masukkan Nama Awal" />
                                            </Form.Item>
                                            <Form.Item label="Nama Akhir">
                                                <Input placeholder="Masukkan Nama Akhir" />
                                            </Form.Item>
                                        </div>
                                        <div className='multiple-input'>
                                            <Form.Item label="Tempat Lahir">
                                                <Input placeholder="Contoh : Bandung" />
                                            </Form.Item>
                                            <Form.Item label="Tanggal Lahir">
                                                <Input placeholder="Contoh : 11/12/21" />
                                            </Form.Item>
                                        </div>
                                        <Form.Item label="Jenis Kelamin">
                                            <Input placeholder="Masukan Jenis Kelamin" />
                                        </Form.Item>
                                    </div>
                                    <div className='button-footer'>
                                        <Button>Save</Button>
                                    </div>
                                </div>
                                
                            </TabPane>
                            <TabPane className='payment-input' tab="Payment Data" key='"2'>
                                <div className='form-input-wrapper'>
                                    <div className='form-input'>
                                        <Form.Item className='valid-input' label="Status Account">
                                            <Input value={"VALID"} placeholder="Dropdown" />
                                        </Form.Item>
                                        <Form.Item label="Payment Name">
                                            <Input value={"I KADEK EVA YOGI PRANATA"} />
                                        </Form.Item>
                                        <Form.Item label="Payment A/C">
                                            <Input value={'2370328431'} />
                                        </Form.Item>
                                        <Form.Item label="Bank Name">
                                            <Input value={'BCA'} />
                                        </Form.Item>
                                        <Form.Item label="Change Bank Account Name">
                                            <Input value={'I KADEK EVA YOGI PRANATA'} />
                                        </Form.Item>
                                    </div>
                                    <div className='button-footer'>
                                        <Button>Save</Button>
                                    </div>
                                </div>
                            </TabPane>
                            <TabPane className="kyc-input" tab="KYC Data" key='"3'>
                                <div className='form-input-wrapper'>
                                    <div className='form-input'>
                                        <Form.Item label="Mobile Phone">
                                            <Input prefix={<DeleteOutlined/>} value={"6281234567890"} placeholder="Dropdown" />
                                        </Form.Item>
                                        <Form.Item label="Email">
                                            <Input prefix={<DeleteOutlined/>} value={"mail@mail.com"} />
                                        </Form.Item>
                                        <Form.Item label="Education">
                                            <Input value={'S1'} />
                                        </Form.Item>
                                        <Form.Item label="Occupation">
                                            <Input value={'Pelajar'} />
                                        </Form.Item>
                                        <Form.Item label="Income Level (IDR)">
                                            <Input value={'> Rp 50 Juta - 100 Juta / tahun'} />
                                        </Form.Item>
                                        <Form.Item label="Investment Objective">
                                            <Input value={'Investment'} />
                                        </Form.Item>
                                    </div>
                                    <div className='button-footer'>
                                        <Button>Save</Button>
                                    </div>
                                </div>
                            </TabPane>
                            <TabPane className='file-edd-input' tab="List File EDD" key='"4'>
                                <div className='form-input-wrapper'>
                                    <div className='form-input'>
                                        <Form.Item label="Last Files">
                                            <Input prefix={<UploadInput/>} value={"6281234567890"} placeholder="Dropdown" />
                                            <Input prefix={<UploadInput/>} value={"mail@mail.com"} />
                                        </Form.Item>
                                    </div>
                                    <div className='button-footer'>
                                    </div>
                                </div>
                            </TabPane>
                        </Tabs>
                    </div>
                </div>
                <Modal className='compare-modal' title="Bandingkan" width={830} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                    <div className='modal-body-wrapper'>
                        <div className='left-content'>
                            <p>Foto KTP</p>
                            <img src={KTP} alt=""/>
                        </div>
                        <div className='right-content'>
                            <Tabs defaultActiveKey={"1"}>
                                <TabPane className='edd-tab' tab="EDD" key='"1'>
                                    <div className='main-photo'>
                                        <img src={MainPhoto} alt=""/>
                                    </div>
                                    <div className='sub-photo'>
                                        <img src={SubPhoto1} alt=""/>
                                        <img src={SubPhoto2} alt=""/>
                                    </div>
                                </TabPane>
                                <TabPane className='signature-tab' tab="Signature" key='"2'>
                                    <img src={Signature} alt=""/>
                                </TabPane>
                            </Tabs>
                        </div>
                    </div>
                </Modal>
            </div>
        )
}

export default WaitingApprovalDetail