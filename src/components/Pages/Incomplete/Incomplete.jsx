import React from 'react'
import '../Incomplete/Incomplete.css'
import { Input, Select, Button, Dropdown, Space, DatePicker, Table, Pagination } from 'antd';
import reload from '../../../assets/reload-icon.svg'
import { SearchOutlined, DownOutlined } from '@ant-design/icons'
import tableList from '../../DataTable/incompleteList'
import tableData from '../../DataTable/incompleteData'
import listPagination from '../../../components/listPagination'

const { Option } = Select

class Incomplete extends React.Component{
    render(){
        return(
            <div className='incomplete-page'>
                <div className='content-header'>
                    <div className='left-content'>
                        <p>Incomplete</p>
                        <span>&#8226;</span>
                        <p>212 Total User</p>
                    </div>
                    <div className='right-content'>
                        <img src={reload} alt=""/>
                        <p>Refresh</p>
                    </div>
                </div>
                <div className='content-table-header'>
                    <div className='left-content'>
                        <p>Search</p>
                    </div>
                    <div className='right-content'>
                    <div className='left-filter-content'>
                        <Select placeholder="Phone" style={{ width: 85 }} onChange={this.handleChange}>
                        <Option value="jack">Jack</Option>
                        <Option value="lucy">Lucy</Option>
                        </Select>
                    </div>
                    <div className='boundary-content' />

                    <Input placeholder='Search for name, invoice, etc' prefix={<SearchOutlined/>}></Input>
                    <div className='boundary-content' />
                    <Space direction="vertical" size={12}>
                        <DatePicker placeholder="Date"/>
                    </Space>
                    <div className='right-filter-content'>
                        <Button type='primary'>Search</Button>
                    </div>
                    </div>
                </div>
                <div className='content-table-body'>
                    <Table 
                        columns={tableList}  
                        dataSource={tableData} 
                        scroll={{ y: 300 }}
                    />
                </div>
                <div className='content-table-pagination'>
                    <Pagination
                        showSizeChanger
                        defaultCurrent={1}
                        total={100}
                    />
                    <Dropdown overlay={listPagination} placement="bottomCenter"  arrow>
                        <Button>10<DownOutlined /></Button>
                    </Dropdown>
                    </div>
            </div>
        )
    }
}

export default Incomplete