import { Menu } from 'antd';

const listPagination = (
    <Menu>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="# ">
          10
        </a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="# ">
          20
        </a>
      </Menu.Item>
    </Menu>
  );

export default listPagination