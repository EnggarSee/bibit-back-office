

const data = [
    {
      key: '1',
      name: 'Change Puspita',
      invoice: 'PI899310VWQT7',
      fundName: 'BNI-AM Dana LANCAR',
      amount: 'Rp 125.000.000',
      channel: 'TRANSFER',
      tags: ['confirmed'],
      transaction: '21 April 2021, 13:30',
      upload: '21 Agustus 2021, 13:30'
    },
    {
      key: '2',
      name: 'Jhon Wick',
      invoice: 'PI899310VWQT7',
      fundName: 'BAHANA LIKUID SYARIAH',
      amount: 'Rp 225.000.000',
      channel: 'TRANSFER',
      tags: ['confirmed'],
      transaction: '21 April 2021, 13:30',
      upload: '21 Agustus 2021, 13:30'
    },
    {
      key: '3',
      name: 'Nana Angela Putri',
      invoice: 'PI899310VWQT7',
      fundName: 'BAHANA MES SYARIAH FUND',
      amount: 'Rp 75.000.000',
      channel: 'TRANSFER',
      tags: ['confirmed'],
      transaction: '21 April 2021, 13:30',
      upload: '21 Agustus 2021, 13:30'
    },
    {
      key: '4',
      name: 'Nindyo Kusumaning',
      invoice: 'PI899310VWQT7',
      fundName: 'BATAVIA DANA KAS MAXIMA',
      amount: 'Rp 25.230.300',
      channel: 'TRANSFER',
      tags: ['confirmed'],
      transaction: '21 April 2021, 13:30',
      upload: '21 Agustus 2021, 13:30'
    },
    {
      key: '5',
      name: 'Sarah Amelia',
      invoice: 'PI899310VWQT7',
      fundName: 'DANAREKSA MELATI',
      amount: 'Rp 125.000.000',
      channel: 'TRANSFER',
      tags: ['confirmed'],
      transaction: '21 April 2021, 13:30',
      upload: '21 Agustus 2021, 13:30'
    },
    {
      key: '6',
      name: 'Ayu Meilani',
      invoice: 'PI899310VWQT7',
      fundName: 'MAJORIS SAHAM ALOKASI',
      amount: 'Rp 235.000.000',
      channel: 'TRANSFER',
      tags: ['confirmed'],
      transaction: '21 April 2021, 13:30',
      upload: '21 Agustus 2021, 13:30'
    },
    {
      key: '7',
      name: 'Enggar Septrinas',
      invoice: 'PI899310VWQT7',
      fundName: 'BNI-AM Dana LANCAR',
      amount: 'Rp 215.000.000',
      channel: 'TRANSFER',
      tags: ['confirmed'],
      transaction: '21 April 2021, 13:30',
      upload: '21 Agustus 2021, 13:30'
    },
    {
      key: '8',
      name: 'Genta Kamsa',
      invoice: 'PI899310VWQT7',
      fundName: 'MANDIRI INVESTA ATRAKTIF',
      amount: 'Rp 295.000.000',
      channel: 'TRANSFER',
      tags: ['confirmed'],
      transaction: '21 April 2021, 13:30',
      upload: '21 Agustus 2021, 13:30'
    },
    {
      key: '9',
      name: 'Irfan Maulana',
      invoice: 'PI899310VWQT7',
      fundName: 'PRINCIPAL CASH FUND',
      amount: 'Rp 252.000.000',
      channel: 'TRANSFER',
      tags: ['confirmed'],
      transaction: '21 April 2021, 13:30',
      upload: '21 Agustus 2021, 13:30'
    },
    {
      key: '10',
      name: 'Bintang Prayoga',
      invoice: 'PI899310VWQT7',
      fundName: 'SUCORINVEST CITRA DANA',
      amount: 'Rp 154.000.000',
      channel: 'TRANSFER',
      tags: ['confirmed'],
      transaction: '21 April 2021, 13:30',
      upload: '21 Agustus 2021, 13:30'
    },
  ];

export default data;