

const columns = [
    {
      title: 'PHONE NUMBER',
      dataIndex: 'phone',
      key: 'phone',
      width: '50%'
    },
    {
      title: 'PROFILING COMPLETION DATE',
      dataIndex: 'date',
      key: 'date',
   
    },
  ];
export default columns;
