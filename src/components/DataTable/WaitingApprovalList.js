import { Tag } from 'antd';


const columns = [
    {
      title: 'PHONE NUMBER',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'EMAIL ADDRESS',
      dataIndex: 'email',
      key: 'email',
    },
    {
        title: 'IS OPEN',
        dataIndex: 'isopen',
        key: 'isopen',
        render: isopen =>(

        isopen === 'Not Opened' ? 
            <Tag className = 'not-opened'>{isopen}</Tag>
            : <Tag className='opened'>{isopen}</Tag>

        ),
    },
    {
        title: 'SUBMIT DATE',
        dataIndex: 'date',
        key: 'date',
    },
    {
        title: 'STATUS',
        dataIndex: 'status',
        key: 'status',
        render: status =>(

            status === 'INCOMPLETE' ? 
                <Tag className = 'not-opened'>{status}</Tag>
                : <Tag className='opened'>{status}</Tag>
    
            ),
      },
  ];
export default columns;
