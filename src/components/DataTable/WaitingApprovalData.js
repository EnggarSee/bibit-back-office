

const data = [
    {
      key: '1',
      phone: '6282271289081',
      email: 'enggar@sample.com',
      isopen: 'Opened by Mahardika',
      date: 'Aug 28 2021, 13:30',
      status: 'INCOMPLETE'
    },
    {
        key: '2',
        phone: '6282271289081',
        email: 'bintang@sample.com',
        isopen: 'Opened by Hendriyanto',
        date: 'Aug 28 2021, 13:30',
        status: 'COMPLETE'
      },
      {
        key: '3',
        phone: '6282271289081',
        email: 'irfan@sample.com',
        isopen: 'Not Opened',
        date: 'Aug 28 2021, 13:30',
        status: 'COMPLETE'
      },
      {
        key: '4',
        phone: '6282271289081',
        email: 'raihan@sample.com',
        isopen: 'Opened by Enggar',
        date: 'Aug 28 2021, 13:30',
        status: 'INCOMPLETE'
      },
      {
        key: '5',
        phone: '6282271289081',
        email: 'jefry@sample.com',
        isopen: 'Opened by Bintang',
        date: 'Aug 28 2021, 13:30',
        status: 'INCOMPLETE'
      },
      {
        key: '6',
        phone: '6282271289081',
        email: 'feri@sample.com',
        isopen: 'Not Opened',
        date: 'Aug 28 2021, 13:30',
        status: 'COMPLETE'
      },
      {
        key: '7',
        phone: '6282271289081',
        email: 'krisna@sample.com',
        isopen: 'Opened by Feri',
        date: 'Aug 28 2021, 13:30',
        status: 'COMPLETE'
      },
  ];

export default data;