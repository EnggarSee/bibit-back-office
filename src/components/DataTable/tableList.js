import { Tag } from 'antd';
import linkAja from '../../assets/link-aja.svg'

const columns = [
    {
      title: 'NAME',
      dataIndex: 'name',
      key: 'name',
      sorter: 'true',
      render: text => <a href='# ' style={{fontSize:'12px', fontWeight:600, color:'#00AB6B'}}>{text}</a>,
    },
    {
      title: 'NO INVOICE',
      dataIndex: 'invoice',
      key: 'invoice',
   
    },
    {
      title: 'FUND NAME',
      dataIndex: 'fundName',
      key: 'fundName',
      sorter: 'true',
      width: '15%',
    },
    {
      title: 'AMOUNT',
      dataIndex: 'amount',
      key: 'amount',
      sorter: 'true',
      width: '10%',
    },
    {
      title: 'CHANNEL',
      dataIndex: 'channel',
      key: 'channel', 
      width: '7%',
      render : channel => <p style={{fontSize:'12px',  paddingTop:'5px' ,fontWeight:600, color:'#00AB6B'}}>{channel}</p>
    },
    {
      title: 'PARTNER',
      dataIndex: 'partner',
      key: 'partner',
      align: 'center',
      width: '6%',
      render: () => <img src={linkAja} style={{width:'23px', height:'23px'}} alt=""/>
    },
    {
      title: 'STATUS',
      key: 'tags',
      dataIndex: 'tags',
      align: 'center',
      render: tags => (
        <>
          {tags.map(tag => {
            let color = tag === 'confirmed' ? '#A3E7B6' : 'red';
            if (tag === 'loser') {
              color = 'volcano';
            }
            return (
              <Tag color={color} key={tag} style={{backgroundColor:'red !important'}}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: 'TRANSACTION DATE',
      dataIndex: 'transaction',
      key: 'transaction',
      sorter: 'true',
      width: '14%'
    },
    {
      title: 'UPLOAD PAYMENT DATE',
      dataIndex: 'upload',
      key: 'upload',
      sorter: 'true',
      width: '14%'
    },
  ];
export default columns;
