import icon1 from '../assets/Iconly 2/Light-Outline/Folder.svg'
import icon2 from '../assets/Iconly 3/Light-Outline/Edit.svg'
import icon3 from '../assets/Iconly 4/Light-Outline/Wallet.svg'
import icon4 from '../assets/Iconly 5/Light-Outline/Activity.svg'
import icon5 from '../assets/Iconly 6/Light-Outline/Bookmark.svg'
import icon6 from '../assets/Iconly 7/Light-Outline/Graph.svg'
import icon7 from '../assets/Iconly 8/Light-Outline/Document.svg'
import icon8 from '../assets/Iconly 9/Light-Outline/Paper-Upload.svg'
import icon9 from '../assets/Iconly 12/Light-Outline/Edit.svg'
import theme1 from '../assets/theme-icon/Edit.svg'
import theme2 from '../assets/theme-icon/Folder.svg'
import theme3 from '../assets/theme-icon/Edit2.svg'
import theme5 from '../assets/theme-icon/Activity.svg'
import theme6 from '../assets/theme-icon/Bookmark.svg'
import theme7 from '../assets/theme-icon/Graph.svg'
import theme9 from '../assets/theme-icon/Document.svg'
import theme10 from '../assets/theme-icon/Paper-Upload.svg'


const SidebarImage = [icon9, icon1, icon2, icon3, icon4, icon5, icon6, icon7, icon8, theme1, theme2, theme3, theme5, theme6, theme7, theme9, theme10]

export default SidebarImage;
