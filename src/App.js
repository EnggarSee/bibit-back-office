import { Layout, Menu, Input, Select, Button, Dropdown, Space, DatePicker, Table, Pagination } from 'antd';
import React from 'react'
import './components/style/backoffice.css'
import { SearchOutlined, DownOutlined } from '@ant-design/icons'
import bibitLogo from './assets/bibit-logo.svg'
import logoDarkMode from './assets/logo-darkmode.svg' 
import avatar from './assets/avatar.svg'
import menu from './assets/menu.svg'
import menuDark from './assets/menu-dark.svg'
import unMenuDark from './assets/non-aktif-dark.svg'
import unmenu from './assets/non-aktif.svg'
import SidebarImage from './components/sidebarIcon'
import reload from './assets/reload-icon.svg'
import linkAja from './assets/link-aja.svg'
import filter from './assets/filter.svg'
import filterDark from './assets/filter-dark-mode.svg'
import tableList from './components/DataTable/tableList'
import tableData from './components/DataTable/tableData'
import listPagination from './components/listPagination'
import FilterContent from './components/filterContent'
import Lottie from 'react-lottie';
import menuSelectedAnimation from './assets/lottie/menu-selected.json'
import Helmet from 'react-helmet';
import star from './assets/star.svg'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from "react-router-dom";
import Incomplete from './components/Pages/Incomplete/Incomplete'
import WaitingApproval from './components/Pages/WaitingApproval/WaitingApproval'
import WaitingApprovalDetail from './components/Pages/WaitingApproval/WaitingApprovalDetail'




const { Option } = Select
const { RangePicker } = DatePicker
const { SubMenu } = Menu;
const { Header, Sider } = Layout;

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: menuSelectedAnimation,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice"
  }
};

function onShowSizeChange(current, pageSize) {
  console.log(current, pageSize);
}

class App extends React.Component {
  
  rootSubmenuKeys = ['sub1', 'sub2', 'sub4'];
  state = {
    collapsed: false,
    filterContent: false,
    theme: false,
    openKeys: ['sub4'],
    width: 0,
    typhography: "1",
    IsLight: true,
    selectedRowKeys: [],
  };
  
  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  filterShow = () => {
    this.setState({
      filterContent: !this.state.filterContent,
    })
  }
  themeSwitch = () =>{
    this.setState({
      theme: !this.state.theme
    })
  }

  handleChange(value) {
    console.log(`selected ${value}`);
  }

  typhographyChange(value){
    this.setState({
      typhography: value,
    })
  }
  componentDidMount(prev,next){
    this.updateWindowDimensions();
    console.log('halo');
    window.addEventListener('resize', this.updateWindowDimensions);
    if(this.state.width !== 0 && this.state.width < 1280){
      this.setState({
        collapsed: true,
        filterContent: false,
      })
    }
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
    
  } 
  updateWindowDimensions = () => {
      this.setState({ width: window.innerWidth});
      if(this.state.width !== 0 && this.state.width < 1280){
        this.setState({
          collapsed: true,
          filterContent: false,
        })
      }
  };
  onOpenChange = openKeys => {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  };

  handleSwitch = (status) => {
    
      this.setState({
        IsLight: !status,
        theme: !this.state.theme
     })
  }

  onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  

  render() {
    const { filterContent, theme, collapsed, openKeys, IsLight, typhography, selectedRowKeys } = this.state
    console.log(this.state.typhography, 'theme')
    console.log(collapsed, 'collapse');
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    return (
      <Router>
      <Layout className={theme === true ?  "layout-dark" : 'layout-light' }>
        <div className={typhography === "1" ? 'type-open-sans' : typhography === "2" ? 'type-lato' : 'type-noto'}>
          <Layout >
            {!collapsed && 
            <Sider trigger={null} width={226} 
            className={theme === false ? "site-layout-background back-office-sider-off" : "site-layout-background back-office-sider"} >
              <div className="sidebar-toogle">
              <img 
                    src={theme === true && collapsed === true ? menuDark : 
                        theme === true && collapsed === false ? unMenuDark : 
                        theme === false && collapsed === true ? menu : unmenu  } alt="" onClick={this.toggle}/>  
              </div>
              <Menu
                mode="inline"
                style={{ borderRight: 0 }}
                openKeys={this.state.openKeys}
                onOpenChange={this.onOpenChange}
                defaultSelectedKeys={['1']}
              >
                <SubMenu key="sub1" icon={openKeys[0] === "sub1" ? <Lottie 
                  options={defaultOptions}
                    height={25}
                    width={25}
                />: null}title="Troubleshoot">
                  <Menu.Item key="11">
                    <Link to="/sidebar" className='right-sub-menu'>
                      <img alt='' src={theme === true ? SidebarImage[6] : SidebarImage[14]}/>
                      <span>Sidebar Menu</span>
                    </Link>
                  </Menu.Item>
                
                </SubMenu>
                <SubMenu key="sub2" icon={openKeys[0] === "sub2" ? <Lottie 
                  options={defaultOptions}
                    height={25}
                    width={25}
                />: null } title="Subscription">
                  <Menu.Item key="10">
                    <div className='right-sub-menu'>
                      <img alt='' src={theme === true ? SidebarImage[4] : SidebarImage[12]}/>
                      <span>Edit Sidebar</span>
                    </div>
                  </Menu.Item>
                </SubMenu>
                <SubMenu key="sub4"  icon={openKeys[0] === "sub4" ? <Lottie 
                    options={defaultOptions}
                    openKeys={"1"}
                    height={25}
                    width={25}
                />: null }title="Registration">
                  <Menu.Item key="1">
                    <Link to='/incomplete' className='right-sub-menu'>
                      <img alt='' src={theme === true ? SidebarImage[0] : SidebarImage[9]}/>
                      <span>Incomplete</span>
                    </Link>
                    <div className='left-sub-menu'>
                      <span>123</span>
                    </div> 
                  </Menu.Item>
                  <Menu.Item key="2">
                    <Link to='/waiting-approval' className='right-sub-menu'>
                      <img alt='' src={theme === true ? SidebarImage[5] : SidebarImage[13]}/>
                      <span>Waiting CS Approval</span>
                    </Link>
                    <div className='left-sub-menu'>
                      <span>12</span>
                    </div>
                  </Menu.Item>
                  <Menu.Item key="3">
                  <div className='right-sub-menu'>
                      <img alt='' src={theme === true ? SidebarImage[2] : SidebarImage[11]}/>
                      <span>New Instant Buy</span>
                    </div>
                    <div className='left-sub-menu'>
                      <span>678</span>
                    </div>
                  </Menu.Item>
                  <Menu.Item key="4">
                  <div className='right-sub-menu'>
                      <img alt='' src={theme === true ? SidebarImage[4] : SidebarImage[12]}/>
                      <span>High Risk</span>
                    </div>
                    <div className='left-sub-menu'>
                      <span>154</span>
                    </div>
                  </Menu.Item>
                  <Menu.Item key="5">
                  <div className='right-sub-menu'>
                      <img alt='' src={theme === true ? SidebarImage[5] : SidebarImage[13]}/>
                      <span>Approved Buy</span>
                    </div>
                    <div className='left-sub-menu'>
                      <span>1132</span>
                    </div>
                  </Menu.Item>
                  <Menu.Item key="6">
                  <div className='right-sub-menu'>
                      <img alt='' src={theme === true ? SidebarImage[6] : SidebarImage[14]}/>
                      <span>Import From S-Invest</span>
                    </div>
                    <div className='left-sub-menu'>
                      <span></span>
                    </div>
                  </Menu.Item>
                  <Menu.Item key="8">
                  <div className='right-sub-menu'>
                      <img alt='' src={theme === true ? SidebarImage[8] : SidebarImage[16]}/>
                      <span>Import Allocation</span>
                    </div>
                    <div className='left-sub-menu'>
                      <span>3838</span>
                    </div>
                  </Menu.Item>
                  <Menu.Item key="9">
                  <div className='right-sub-menu'>
                      <img alt='' src={theme === true ? SidebarImage[1] : SidebarImage[10]}/>
                      <span>Accepted Buy</span>
                    </div>
                    <div className='left-sub-menu'>
                      <span>188</span>
                    </div>
                  </Menu.Item>
                  <Menu.Item key="10">
                  <div className='right-sub-menu'>
                      <img alt='' src={theme === true ? SidebarImage[1] : SidebarImage[10]}/>
                      <span>Rejected Buy</span>
                    </div>
                    <div className='left-sub-menu'>
                      <span></span>
                    </div>
                  </Menu.Item>
                </SubMenu>
              </Menu>
            </Sider>}
            <Layout className='ant-layout-content'>
            <Helmet bodyAttributes={theme === true ? {style: 'background-color : #2E2E2E !important'} : {style: 'background-color : #f0f2f5'}} />
            <div className={theme === false ? 'back-office-header' : 'back-office-header dark-mode' }>
              <Header className={collapsed? "header-collpase" : "header"}>
              <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                <Menu.Item className='left-side-menu' key="1">
                  <div className='left-side-menu-wrapper'>
                  <div className={collapsed ? "sidebar-toogle-show": "sidebar-toogle"}>
                  <img 
                      src={theme === true && collapsed === true ? menuDark : 
                      theme === true && collapsed === false ? unMenuDark : 
                      theme === false && collapsed === true ? menu : unmenu  } alt="" onClick={this.toggle}/>  
                  </div>
                    <img src={theme === false ? bibitLogo : logoDarkMode} alt="logo-bibit"/>
                    <span className='back-office-text'>&#8226; Back Office</span>
                  </div>
                  <div className='center-side-menu-wrapper'>
                    <Input placeholder='what are you looking for?' prefix={<SearchOutlined/>} />
                  </div>
                  <div className='right-side-menu-wrapper'>
                  <div className="switch-typhography">
                          <Select placeholder="Pilih font" style={{ width: 105 }} onChange={(e)=> this.typhographyChange(e)}>
                            <Option value="1">Open Sans</Option>
                            <Option value="2">Lato</Option>
                            <Option value="3">Noto</Option>
                          </Select>
                      </div>
                  <div
                      className={`sw-wrapper ${IsLight ? "" : "sw-wrapper-dark"}`}
                      onClick={() => this.handleSwitch(IsLight)}
                    >
                      <div
                        className={`sw-knob-mode ${
                          IsLight
                            ? "sw-light-mode sw-opacity-show"
                            : "sw-dark-mode sw-opacity-hide"
                        }`}
                      ></div>
                      <div
                        className={`sw-knob-dark-mode ${
                          IsLight
                            ? "sw-opacity-hide"
                            : "sw-opacity-show sw-knob-dark-mode-reupdate"
                        }`}
                      ></div>
                      <img
                        src={star} alt=""
                        className={`sw-star-dark ${
                          IsLight ? "sw-opacity-hide" : "sw-star-dark-opacity-show"
                        }`}
                      />
                    </div>
                    <p>feri@stockbit.com</p>
                    <img className='ava-image' src={avatar} alt=""/>
                    <DownOutlined/>
                  </div>
                </Menu.Item>
              </Menu>
              </Header>
            </div>
            <Switch>
              <Route path='/sidebar'>
              <div className='combine-content'>
            <div className={filterContent === false ? "site-layout-background back-office-content unfilter" : "site-layout-background back-office-content"}>
              <div className='left-content-primary'>
                <div className='content-header'>
                    <div className='left-content'>
                      <p>New Payment</p>
                      <span>&#8226;</span>
                      <p>388</p>
                    </div>
                    <div className='right-content'>
                      <img src={reload} alt=""/>
                      <p>Refresh</p>
                    </div>
                  </div>
                  <div className='content-amount'>
                    <div className='left-content'>
                      <p>Total Amount</p>
                      <p>Rp61.810.913</p>
                    </div>
                    <div className='right-content'>
                      <Button type='primaty'>Import <img src={linkAja} alt=""/></Button>
                      <Button type='primaty' onClick={this.filterShow}><img src={theme=== true ? filterDark :filter} alt=""/></Button>
                    </div>
                  </div>
                  <div className='main-wrapper'>

                  <div className={collapsed === false && filterContent === true ? 'content-table-header flex' : collapsed===false || filterContent===true ? 'content-table-header uncollapsed' : 'content-table-header'}>
                    <div className='left-content'>
                      <p>6 Total Confirmed Payment</p>
                    </div>
                    <div className='right-content'>
                      <div className='left-filter-content'>
                        <Input placeholder='Search for name, invoice, etc' prefix={<SearchOutlined/>}></Input>
                        <div className='boundary-content' />
                        <Space direction="vertical" size={12}>
                          <RangePicker placeholder={['Start Date', 'End Date']}/>
                        </Space>
                        <div className='boundary-content' />
                        <Select placeholder="Nama" style={{ width: 85 }} onChange={this.handleChange}>
                        <Option value="jack">Jack</Option>
                        <Option value="lucy">Lucy</Option>
                      </Select>
                      </div>
                      <div className='right-filter-content'>
                        <Button type='primary'>Search</Button>
                      </div>
                    </div>
                  </div>
                  <div className={collapsed === false && filterContent === true ? 'content-table-body uncollapsed filter' : collapsed===false || filterContent===true ?' content-table-body uncollapsed'  : 'content-table-body'}>
                    <Table 
                      columns={tableList}  
                      dataSource={tableData} 
                      scroll={{ x: 1500, y: 300 }}
                      rowSelection={rowSelection}/>
                  </div>
                  <div className='content-table-pagination'>
                  <Pagination
                    showSizeChanger
                    onShowSizeChange={onShowSizeChange}
                    defaultCurrent={1}
                    total={100}
                  />
                  <Dropdown overlay={listPagination} placement="bottomCenter"  arrow>
                    <Button>10<DownOutlined /></Button>
                  </Dropdown>
                  </div>
                  </div>

              </div>
              </div>
              <div className={theme === true ? 'filter-content-wrapper dark-mode':'filter-content-wrapper'} style={{display: filterContent===true ? 'block': 'none'}}>
                  <div className='content-filter-container'>
                    <div className='top-content-wrapper'>
                      <div className='filter-content-show'>
                          <div className='filter-content-header'>
                              <p>Filter</p>
                              <p onClick={this.filterShow}>CLOSE</p>
                          </div>
                      </div>
                    <FilterContent action={this.filterShow}  />
                    </div>
                    <div className='filter-footer'>
                          <Button type='default'>RESET</Button>
                    </div> 
                  </div>
              </div>
            </div>
              </Route>
              <Route path='/incomplete'>
                <Incomplete/>
              </Route>
              <Route path='/waiting-approval'>
                <WaitingApproval/>
              </Route>
              <Route path='/waiting-approval-detail'>
                <WaitingApprovalDetail/>
              </Route>
            </Switch>
            </Layout>
          </Layout>
        </div>
    </Layout>
    </Router>
    );
  }
}

export default App;
